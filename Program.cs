﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace JobStuff
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var words = File.ReadAllLines("words.txt");
            Play(new RoundState
            {
                WordSize = 5,
                AllowedPotentiallyMatchingWords = words.ToList()
            });
        }

        public class RoundState
        {
            public int WordSize;
            public List<string> AllowedPotentiallyMatchingWords;
        }
        
        private static void Play(RoundState constraints)
        {
            Console.WriteLine($"The word to guess has {constraints.WordSize} letters.");
            
            GuessState state = new GuessState(constraints.WordSize);
            Console.WriteLine(string.Join(" ", state.SeenCorrectLetterAndPlace));
            
            while (true)
            {
                Console.WriteLine("Please enter your guess");

                string guess;
                while (true)
                {
                    guess = Console.ReadLine();
                    if (string.IsNullOrWhiteSpace(guess))
                        continue;

                    guess = guess.ToLower().Trim();
                    if (guess.Length != constraints.WordSize)
                    {
                        Console.WriteLine($"You guess has {guess.Length} letters, the searched word has {constraints.WordSize} letters, try again.");
                        continue;
                    }

                    break;
                }

                var fb = MakeFeedbackWithLeastInformation(constraints, state, guess);
                
                if (guess == fb.TargetWord)
                {
                    Console.WriteLine($"Congratulations! {guess} is correct!");
                    break;
                }

                state = AcknowledgeFeedback(state, fb, guess);
                
                var sb = new StringBuilder(string.Join(" ", fb.CorrectLetterAndPlace));

                if (fb.MisplacedLetters.Count > 0)
                {
                    sb.Append(" : ").Append(string.Join(", ", fb.MisplacedLetters.OrderBy(a => a)));
                }

                Console.WriteLine(sb.ToString());
            }
        }

        public static Feedback MakeFeedbackWithLeastInformation(RoundState c, GuessState state, string input)
        {
            Feedback lowestScoreFeedback = null;
            int lowestScore = int.MaxValue;
            
            for (var i = 0; i < c.AllowedPotentiallyMatchingWords.Count; i++)
            {
                var word = c.AllowedPotentiallyMatchingWords[i];

                if (!WordSatisfiesState(state, word))
                {
                    c.AllowedPotentiallyMatchingWords.RemoveAt(i--);
                    continue;
                }

                var (a, b) = Match(word, input);

                var score = Score(a, b);

                if (score < lowestScore)
                {
                    lowestScoreFeedback = new Feedback {CorrectLetterAndPlace = a, MisplacedLetters = b, TargetWord = word};
                    lowestScore = score;
                }
            }

            return lowestScoreFeedback;
        }

        private static bool WordSatisfiesState(GuessState state, string word)
        {
            for (int i = 0; i < state.SeenCorrectLetterAndPlace.Length; i++)
            {
                if (state.SeenCorrectLetterAndPlace[i] != '_' && word[i] != state.SeenCorrectLetterAndPlace[i])
                    return false;

                if (state.WrongLetters[i].Contains(word[i]))
                    return false;
            }
            
            foreach (var attempt in state.Attempts)
            {
                foreach (var letter in attempt.fb.MisplacedLetters)
                {
                    var countInCorrect = attempt.fb.CorrectLetterAndPlace.Count(a => a == letter);
                    var countInNewWord = word.Count(a => a == letter);

                    if (countInNewWord < countInCorrect + 1)
                        return false;
                }

                var (_, misplaced) = Match(word, attempt.input);

                if (misplaced.Count > 0)
                {
                    foreach (var c in misplaced)
                    {
                        if (attempt.fb.MisplacedLetters.Contains(c))
                        {
                            continue;
                        }
                        
                        return false;
                    }
                }
            }
            
            return true;
        }

        public static GuessState AcknowledgeFeedback(GuessState oldState, Feedback latest, string input)
        {
            var state = new GuessState(oldState);

            for (int i = 0; i < latest.CorrectLetterAndPlace.Length; i++)
            {
                if (state.SeenCorrectLetterAndPlace[i] == '_' && latest.CorrectLetterAndPlace[i] != '_')
                    state.SeenCorrectLetterAndPlace[i] = latest.CorrectLetterAndPlace[i];
                
                if(latest.CorrectLetterAndPlace[i] == '_')
                    state.WrongLetters[i].Add(input[i]);
            }
            
            state.Attempts.Add((latest, input));
            
            return state;
        }

        public class GuessState
        {
            public List<(Feedback fb, string input)> Attempts;
            public char[] SeenCorrectLetterAndPlace;
            public HashSet<char>[] WrongLetters;

            public GuessState(int constraintsWordSize)
            {
                Attempts = new List<(Feedback fb, string input)>();
                SeenCorrectLetterAndPlace = new char[constraintsWordSize];
                WrongLetters = new HashSet<char>[constraintsWordSize];
                
                for (int i = 0; i < constraintsWordSize; i++)
                {
                    SeenCorrectLetterAndPlace[i] = '_';
                    WrongLetters[i] = new HashSet<char>();
                }
            }

            public GuessState(GuessState oldState)
            {
                Attempts = oldState.Attempts.ToList();
                SeenCorrectLetterAndPlace = (char[])oldState.SeenCorrectLetterAndPlace.Clone();
                WrongLetters = new HashSet<char>[oldState.SeenCorrectLetterAndPlace.Length];
                for (int i = 0; i < oldState.SeenCorrectLetterAndPlace.Length; i++)
                {
                    WrongLetters[i] = oldState.WrongLetters[i].ToHashSet();
                }
            }
        }
        
        public class Feedback
        {
            public char[] CorrectLetterAndPlace;
            public HashSet<char> MisplacedLetters;
            public string TargetWord;
        }

        public static int Score(char[] correctLetterAndPlace, HashSet<char> misplacedLetters)
        {
            return correctLetterAndPlace.Count(a => a != '_') * 10 + misplacedLetters.Count * 3;
        }
        
        public static (char[] correctLetterAndPlace, HashSet<char> misplacedLetters) Match(string searchedWord, string input)
        {
            if(searchedWord.Length != input.Length)
                throw new ArgumentException();

            var correctPlaceAndLetter = new char[searchedWord.Length];
            var correctLetters = new HashSet<char>();

            for (int i = 0; i < searchedWord.Length; i++)
            {
                if (searchedWord[i] == input[i])
                {
                    correctPlaceAndLetter[i] = searchedWord[i];
                }
                else
                {
                    correctPlaceAndLetter[i] = '_';
                }
            }
            
            for (int i = 0; i < searchedWord.Length; i++)
            {
                if (searchedWord[i] == input[i])
                {
                    continue;
                }
                
                if(correctLetters.Contains(input[i]))
                    continue;

                for (int j = 0; j < searchedWord.Length; j++)
                {
                    if(searchedWord[j] != input[i])
                        continue;
                    
                    if(correctPlaceAndLetter[j] != '_')
                        continue;

                    correctLetters.Add(input[i]);
                    break;
                }
            }
            
            return (correctPlaceAndLetter, correctLetters);
        }
    }
}